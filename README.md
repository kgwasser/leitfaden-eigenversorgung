# Leitfaden Eigenversorgung im Feld

Kompetenzgruppe Wasser, Ingenieure ohne Grenzen e. V.

[![build status](https://gitlab.com/kgwasser/leitfaden-probenahme/badges/master/build.svg)](https://gitlab.com/kgwasser/leitfaden-eigenversorgung/commits/master)

## Überblick

Dies ist das Repository für den Leitfaden "Eigenversorgung im Feld" der KG Wasser. Im `master` Branch ist die offizielle Version, im `draft` Branch wird der Leitfaden weiterentwickelt.

Aktuelle PDF Version: http://kgwasser.gitlab.io/leitfaden-eigenversorgung/eigenversorgung.pdf